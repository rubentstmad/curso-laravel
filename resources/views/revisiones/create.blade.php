@extends('layout')

@section('titulo','Áñadir revisión')

@section('contenido')
	{{$errors}}
	<form action="{{route('revisiones.store')}}" method="POST">
		@csrf
		Título: <br><input name="titulo" value="{{old('titulo')}}"><br>
		Fecha inicio:<br> <input type="date" name="fecha_inicio" value="{{old('fecha_inicio')}}"><br>
		Fecha fin:<br> <input type="date" name="fecha_fin" value="{{old('fecha_fin')}}"><br>
		Kilómetros:<br> <input name="kilometros" value="{{old('kilometros')}}"><br>
		Precio:<br> <input name="precio" value="{{old('precio')}}"><br>
		Descripción: <br><textarea name="descripcion" cols=40 rows=5>{{old('descripcion')}}</textarea><br><br>
		<button>Crear</button>
	</form>
@endsection