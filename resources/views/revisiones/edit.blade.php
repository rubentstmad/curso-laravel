@extends('layout')

@section('titulo','Editar revisión \''.$revision->titulo.'\'')

@section('contenido')
	{{$errors}}
	<form action="{{route('revisiones.update',$revision)}}" method="POST">
		@csrf @method('PATCH')
		Título: <br><input name="titulo" value="{{old('titulo',$revision->titulo)}}"><br>
		Fecha inicio:<br> <input type="date" name="fecha_inicio" value="{{old('fecha_inicio',$revision->fecha_inicio)}}"><br>
		Fecha fin:<br> <input type="date" name="fecha_fin" value="{{old('fecha_fin',$revision->fecha_fin)}}"><br>
		Kilómetros:<br> <input name="kilometros" value="{{old('kilometros',$revision->kilometros)}}"><br>
		Precio:<br> <input name="precio" value="{{old('precio',$revision->precio)}}"><br>
		Descripción: <br><textarea name="descripcion" cols=40 rows=5>{{old('descripcion',$revision->descripcion)}}</textarea><br><br>
		<button>Actualizar</button>
	</form>
@endsection