@extends('layout')

@section('titulo','Lista de revisiones')

@section('contenido')
	@isset($revisionesActuales)
		<ul>
			@foreach($revisionesActuales as $revision)
				<li><a href="{{route('revisiones.show',$revision)}}">{{$revision->titulo}}</a></li>
			@endforeach
		</ul>
	@endisset

	<a href="{{route('revisiones.create')}}">Añadir nueva revisión</a>
@endsection