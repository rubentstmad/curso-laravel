@extends('layout')

@section('titulo','Revisión \''.$revision->titulo.'\'')

@if(session('status'))
	{{session('status')}}
@endif
@section('contenido')
	<ul>
		<li>Fecha inicio: {{$revision->fecha_inicio}}</li>
		<li>Fecha fin: {{$revision->fecha_fin}}</li>
		<li>Kilómetros: {{$revision->kilometros}}</li>
		<li>Precio: {{$revision->precio}}€</li>
		<li>Descripción: {{$revision->descripcion}}</li>
	</ul>
	<a href="{{route('revisiones.edit',$revision)}}">editar</a>
	<a href="#" onclick="event.preventDefault();borrarRevision.submit()">eliminar</a>
	<form id='borrarRevision' action="{{route('revisiones.destroy',$revision)}}" method="POST">
		@csrf @method('DELETE')
	</form>
	<a href="{{route('revisiones.index')}}">volver</a>
@endsection