<?php

use Illuminate\Support\Facades\Route;

Route::get('/revisiones','App\Http\Controllers\RevisionesController@index')->name('revisiones.index');
Route::get('/revisiones/crear','App\Http\Controllers\RevisionesController@create')->name('revisiones.create');
Route::post('/revisiones/crear','App\Http\Controllers\RevisionesController@store')->name('revisiones.store');
Route::get('/revisiones/{revision}','App\Http\Controllers\RevisionesController@show')->name('revisiones.show');

Route::get('/revisiones/edit/{revision}','App\Http\Controllers\RevisionesController@edit')->name('revisiones.edit');
Route::patch('/revisiones/edit/{revision}','App\Http\Controllers\RevisionesController@update')->name('revisiones.update');
Route::delete('/revisiones/{revision}','App\Http\Controllers\RevisionesController@destroy')->name('revisiones.destroy');






