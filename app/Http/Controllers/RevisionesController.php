<?php

namespace App\Http\Controllers;

use App\Models\Revisiones;
use Illuminate\Http\Request;

class RevisionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('revisiones.index',[
            'revisionesActuales' => Revisiones::get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('revisiones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $fields=request()->validate([
            'titulo' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'kilometros' => 'required',
            'precio' => 'required',
            'descripcion' => 'required'
        ]);

        $fields['url_friendly']=strtolower(str_replace(" ", "-", $fields['titulo']));

        $nuevaRevision = Revisiones::create($fields);

        return redirect()->route('revisiones.show',$nuevaRevision);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Revisiones $revision)
    {
        return view('revisiones.show',[
            'revision' => $revision
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Revisiones $revision)
    {
        return view('revisiones.edit',[
            'revision' => $revision
        ]); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Revisiones $revision)
    {
        $fields=request()->validate([
            'titulo' => 'required',
            'fecha_inicio' => 'required',
            'fecha_fin' => 'required',
            'kilometros' => 'required',
            'precio' => 'required',
            'descripcion' => 'required'
        ]);

        $revision->update($fields);

        return redirect()->route('revisiones.show',$revision)->with('status','Revisión actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Revisiones $revision)
    {
        $revision->delete();

        return redirect()->route('revisiones.index');
    }
}
