<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Revisiones extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

	public function getRouteKeyName()//Extiende de Model. Sobreeescribeel método getRouteKeyName. la clave de búsqueda en la base de datos se escribe en el return. En este caso, como queremos buscar por defecto por el campo 'url_friendly', debemos retornar ese string para que busque ese campo en el base de datos
	{
		return 'url_friendly';
	}

}
